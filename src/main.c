#include <mpi/mpi.h> //I'm only sure about windows where its <mpi.h>, I think its <mpi/mpi.h> on linux
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#define size 262144

MPI_Datatype dt_point;

MPI_Datatype dt_circle;

typedef struct Point
{
	double x;
	double y;
} Point;



typedef struct Circle
{
	Point center;
	double radius;
} Circle;

/*
* Checks if a given Point is in a given Circle
*/
int is_inside(Circle* circle, Point* point);

/*
* Returns the distance between 2 Points
*/
double distance(Point* a, Point* b);



/*
* Seeds a Point array
*/
void seed(Point* pointsPart,int ntasks,Point* points);

/*
* Checks if the circle is valid
*/
int is_valid_circle(Point* points, Circle* circle, int ntasks, int rank);





int main(int argc, char* argv[])
{
	

	int rank,ntasks;
	int result = 0;

	double startTime, endTime;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

	MPI_Type_contiguous(2, MPI_DOUBLE, &dt_point);
	MPI_Type_commit(&dt_point);

	
	MPI_Type_contiguous(3, MPI_DOUBLE, &dt_circle);
	MPI_Type_commit(&dt_circle);

	srand(time(0)+rank);

	if (rank == 0)
	{
		startTime = MPI_Wtime();
		printf("Generating random numbers..\n");
		fflush(stdout);
	}

	Point* points;
	MPI_Alloc_mem(size * sizeof(Point), MPI_INFO_NULL, &points);

	if (points == NULL)
	{
		printf("\nCouldn't allocate memory to [points]");
		printf("\nTry a smaller size!\n");
		return -1;
	}

	Point* pointsPart;
	pointsPart = malloc(size / ntasks * sizeof(Point));

	if (pointsPart == NULL)
	{
		printf("\nCouldn't allocate memory to [pointsPart]");
		printf("\nTry a smaller size!\n");
		return -1;
	}

	//I use these later in the maxdistance, maxindice part, because this make sure the maxDistance stays bonded to the maxIndice during MPI operations
	Point* maxArray;
	MPI_Alloc_mem(ntasks * 3 * sizeof(Point), MPI_INFO_NULL, &maxArray);

	if (maxArray == NULL)
	{
		printf("\nCouldn't allocate memory to [maxArray]");
		return -1;
	}

	Point* maxArrayPart;
	maxArrayPart = malloc( 3 * sizeof(Point));

	if (maxArrayPart == NULL)
	{
		printf("\nCouldn't allocate memory to [maxArrayPart]");
		return -1;
	}

	//This IS paralellized.
	seed(pointsPart,ntasks,points);


	//This has to be a double to avoid a int-int=>int division later
	double K = 1000.0;

	Circle enclosingCircle;

	enclosingCircle.center.x = 4500;
	enclosingCircle.center.y = 4500;


	if (rank == 0)
	{
		printf("Circle searching..\n");
		fflush(stdout);
	}
	
	//This IS paralellized
	for (int k = 0; k < K; k++)
	{
		//wiping maxArray
		for (int p = 0; p < (3 * ntasks); p++)
		{
			maxArray[p].x = 0;
			maxArray[p].y = -1;
		}
		
		
		double maxDistances[3] = { 0.0, 0.0, 0.0 };
		int maxIndices[3] = { -1, -1, -1 };

		maxArrayPart[0].x = 0;
		maxArrayPart[1].x = 0;
		maxArrayPart[2].x = 0;

		maxArrayPart[0].y = -1;
		maxArrayPart[1].y = -1;
		maxArrayPart[2].y = -1;

		
		for (int i = rank; i < size; i = i + ntasks)
		{
			double dist = distance(&enclosingCircle.center, &points[i]);

			int j = 0;
			while (j < 3 && maxArrayPart[j].x > dist)
			{
				j++;
			}
			if (j < 3)
			{
				maxArrayPart[j].x = dist;
				maxArrayPart[j].y = i;
			}

		}

		
		MPI_Gather(maxArrayPart, 3, dt_point, maxArray, 3, dt_point, 0, MPI_COMM_WORLD);

		if (rank == 0)
		{
			//selecting the 3 max from the smaller array of maximums
			
			for (int l = 0; l < (ntasks * 3); l++)
			{
				int m = 0;
				while (m < 3 && maxDistances[m] > maxArray[l].x)
				{
					m++;
				}
				if (m < 3)
				{
					maxDistances[m] = maxArray[l].x;
					maxIndices[m] = maxArray[l].y;

				}
			}
			
			double maxDifference = (maxDistances[0] - maxDistances[2]) * (1 - (k / K));
			double diffX = (points[maxIndices[0]].x - enclosingCircle.center.x) / maxDistances[0];
			double diffY = (points[maxIndices[0]].y - enclosingCircle.center.y) / maxDistances[0];


			enclosingCircle.center.x += diffX * maxDifference;
			enclosingCircle.center.y += diffY * maxDifference;
			enclosingCircle.radius = maxDistances[0];

			
		}
		
		
		//syncing the circle with the other threads
		MPI_Bcast(&enclosingCircle, 1, dt_circle, 0, MPI_COMM_WORLD);
	}
	fflush(stdout);
	if (rank == 0)
	{
		
		printf("The smallest enclosing circle:\n");
		printf("center x: %f \n", enclosingCircle.center.x);
		printf("center y: %f \n", enclosingCircle.center.y);
		printf("radius: %f \n", enclosingCircle.radius);
	}
		
	MPI_Bcast(points, size, dt_point, 0, MPI_COMM_WORLD);

	if (rank == 0)
	{
		printf("Validating circle..\n");
		fflush(stdout);
	}

	//the validation method IS paralellized
	if (is_valid_circle(points, &enclosingCircle,ntasks,rank))
	{
		
		int approval = 1;
		MPI_Reduce(&approval, &result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
		
	}

	if (rank == 0)
	{
		if (result == ntasks)
		{
			printf("valid circle\n");
			
		}
		
	}
	
	MPI_Free_mem(points);
	MPI_Free_mem(maxArray);
	free(pointsPart);
	free(maxArrayPart);

	if (rank == 0)
	{
		endTime = MPI_Wtime();
		printf("elapsed time: %f", endTime - startTime);
	}

	MPI_Finalize();

	
	return 0;

}








void seed(Point* pointsPart,int ntasks,Point* points)
{

	


	//waste variable to give the random function more time, making its distribution much better.
	int waste;
	//void supresses the warning.
	(void)waste;
	for (int i = 0; i < size/ntasks; i++)
	{
		waste = rand();
		pointsPart[i].x = rand() % 10000;
		pointsPart[i].y = rand() % 10000;

	}
	//MPI_Gather(pointsPart, size / ntasks, dt_point , points, size / ntasks, dt_point, 0, MPI_COMM_WORLD);
	MPI_Allgather(pointsPart, size / ntasks, dt_point, points, size / ntasks, dt_point, MPI_COMM_WORLD);
	
}

double distance(Point* a, Point* b)
{
	return sqrt(pow(a->x - b->x, 2) + pow(a->y - b->y, 2));
}


int is_inside(Circle* circle, Point* point)
{
	// *0.99 to compensate for margin of error from floating point inaccuracies
	if (distance(&circle->center, point) * 0.99 <= circle->radius)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}






int is_valid_circle(Point* points, Circle* circle,int ntasks, int rank)
{
	for (int i = (size / ntasks) * (rank); i < (size/ntasks)*(rank+1); i++)
	{
		//printf("%f \n",distance(&circle->center,&points[i]));
		if (is_inside(circle, &points[i]) == 1)
		{

		}
		else
		{
			return 0;
		}


	}
	return 1;

}